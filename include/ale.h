/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _ALE_H
#define _ALE_H

#include <stdio.h>
#include <stdarg.h>

/*
 * libale requires OpenCL
 *
 * http://www.khronos.org/registry/cl/
 *
 * We assume the following header file, according to official documentation
 * example code.  If this doesn't work, try OpenCL/cl.h (or perhaps CL/cl.h),
 * as even Khronos does not seem to be entirely consistent in OpenCL header
 * naming.
 */

#include <OpenCL/opencl.h>

/*
 * Macro for code definition.
 */

#define ALE_CODE(ID, ...) \
	static const char *ale_string_ ## ID = #__VA_ARGS__ ;

/*
 * Stringification macro.
 */

#define ALE_STRINGIFY(ID) \
	ale_string_ ## ID

#if 0

/*
 * Alternative code definition and stringification macros (e.g., if the above
 * cause compiler warnings regarding unused statics.
 */

#define ALE_CODE(ID, ...) \
	inline const char *ale_stringify_ ## ID() { return #__VA_ARGS__ ; }

#define ALE_STRINGIFY(ID) \
	ale_stringify_ ## ID()

#endif

/*
 * Macros for shared data types used in evaluation.
 */

#define ALE_DATA_TYPE(TYPE, ...) \
	__VA_ARGS__ \
	ALE_CODE(TYPE, __VA_ARGS__ )

#define ALE_STRUCT(TYPE, ...) \
	ALE_DATA_TYPE(TYPE, struct TYPE { __VA_ARGS__ }; )

#define ALE_UNION(TYPE, ...) \
	ALE_DATA_TYPE(TYPE, union TYPE { __VA_ARGS__ }; )

#define ALE_TYPEDEF(TYPE, ...) \
	ALE_DATA_TYPE(TYPE, typedef __VA_ARGS__ TYPE; )

/*
 * Pointer for portable storage (e.g., between 32- and 64-bit architectures).
 * This may allow, e.g., storing compute device pointers on the host, which
 * could be useful for inclusion in structures to be passed back to the compute
 * device, etc.
 *
 * It is expected that TYPE is incomplete on the host side in the common case,
 * but its inclusion may be useful for type checking.  On the other hand,
 * dereferencing P on the host side may have unexpected result, and should not
 * generally be necessary.
 *
 * Note that this should also be usable for storing pointers dereferenced solely
 * on the host side (as might be expected for ale_context, or for its included
 * cl_context).
 *
 * We add a table type (T) here to allow the possibility that pointer values
 * change on the device for a given cl_mem object.  Cf:
 *
 * http://www.khronos.org/message_boards/viewtopic.php?p=4782#p4782
 *
 * INDEX is a (32- or) 64-bit pointer to the index of an (e.g., cl_mem) entry
 * in a table on the host (e.g., in such a table associated with an
 * ale_context).  OFFSET is measured from the start of the indexed memory area,
 * and is disabled for now to reduce pointer sizes, but can be enabled for
 * greater pointer generality.
 */

#define ALE_POINTER(TYPE, TYPEP) \
	ALE_DATA_TYPE( TYPEP, typedef union { TYPE *p; struct {cl_long index; /* cl_int offset; */} t; cl_long widest_pointer_type; } TYPEP;)

/*
 * Macro for API type definition
 *
 * API type name selection is based roughly on the approach taken in 
 *
 * http://www.khronos.org/registry/cl/api/1.0/cl.h
 *
 * *_retain() and *_release() are patterned after OpenCL reference counting.
 * 
 * *_valid() attempts to determine validity, returning non-zero if valid.
 */

#define ALE_API_TYPEDEF(TYPENAME) \
	ALE_DATA_TYPE( _ ## TYPENAME, struct _ ## TYPENAME; ) \
	ALE_POINTER( struct _ ## TYPENAME , TYPENAME ) \
	int TYPENAME ## _valid(TYPENAME xx_valid_argument); \
	TYPENAME TYPENAME ## _NULL(); \
	int TYPENAME ## _retain(TYPENAME xx_retain_argument); \
	int TYPENAME ## _release(TYPENAME xx_release_argument);

/*
 * Macros for API parameter declaration.
 */

#define ALE_API_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_TYPE OBJECT_TYPE ## _get_ ## PARAMETER_NAME (OBJECT_TYPE object);

#define ALE_API_PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	int OBJECT_TYPE ## _set_ ## PARAMETER_NAME (OBJECT_TYPE object, PARAMETER_TYPE value);

#define ALE_API_PARAMETER_RW(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	ALE_API_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	ALE_API_PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE)

#define ALE_API_RC_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_TYPE OBJECT_TYPE ## _retain_ ## PARAMETER_NAME (OBJECT_TYPE object);

#define ALE_API_RC_PARAMETER_RW(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	ALE_API_RC_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	ALE_API_PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE)

/*
 * API types
 */

ALE_API_TYPEDEF(ale_context)

ALE_API_TYPEDEF(ale_image)
ALE_API_TYPEDEF(ale_render)
ALE_API_TYPEDEF(ale_trans)
ALE_API_TYPEDEF(ale_align_properties)

ALE_API_TYPEDEF(ale_filter)
ALE_API_TYPEDEF(ale_psf)

ALE_API_TYPEDEF(ale_certainty)

ALE_API_TYPEDEF(ale_exclusion_list)

ALE_API_TYPEDEF(ale_sequence)

ALE_API_TYPEDEF(ale_scene)


/*
 * Bayer patterns.  (Values are based on original ALE d2::image code, but using
 * standard clockwise ordering rather than ALE counterclockwise ordering.)
 */

enum {
	ALE_BAYER_NONE = 0x0,
	ALE_BAYER_RGBG = 0x4,	     /* binary 100 */
	ALE_BAYER_GRGB = 0x5,	     /* binary 101 */
	ALE_BAYER_GBGR = 0x6,	     /* binary 110 */
	ALE_BAYER_BGRG = 0x7	     /* binary 111 */
};

/*
 * Bayer utility functions.  (Based on original ALE d2::image_bayer_ale_real
 * code.)
 */

/*
 * Return the color of a given pixel, for a given bayer pattern.
 */
static unsigned int ale_bayer_color(unsigned int i, unsigned int j, unsigned int bayer) {
	unsigned int r_x_offset = bayer & 0x1;
	unsigned int r_y_offset = (bayer & 0x2) >> 1;

	return (i + r_y_offset) % 2 + (j + r_x_offset) % 2;
}

/*
 * Return a vector indicating set channels.
 */
static char ale_get_channels(unsigned int i, unsigned int j, unsigned int bayer) {
	if (bayer == ALE_BAYER_NONE)
		return 0x7;
	return (1 << ale_bayer_color(i, j, bayer));
}

/*
 * Indicate whether a given channel exists.
 */
static int ale_has_channel(unsigned int i, unsigned int j, unsigned int k, unsigned int bayer) {
	return (1 << k) & ale_get_channels(i, j, bayer);
}



/*
 * Image formats.
 */

enum {
	ALE_IMAGE_Y,
	ALE_IMAGE_RGB,
	ALE_IMAGE_WEIGHTED_RGB
};

/*
 * Domain types.
 */

enum {
	ALE_TYPE_UINT_8,
	ALE_TYPE_UINT_16,
	ALE_TYPE_UINT_32,
	ALE_TYPE_UINT_64,
	ALE_TYPE_FLOAT_32,
	ALE_TYPE_FLOAT_64
};

/*
 * Invariant types.
 *
 * For ALE avgf, use MEAN|FIRST with saturation threshold;
 * other invariants are analogous; e.g.,
 *
 *   MEAN|LAST,
 *   MEDIAN|FIRST,
 *   etc.
 *
 * Not all expressible combinations are accepted, or, indeed, meaningful.
 */

enum {
	ALE_INVARIANT_NONE = 0,
	ALE_INVARIANT_MEAN = 1,
	ALE_INVARIANT_FIRST = 2,
	ALE_INVARIANT_LAST = 4,
	ALE_INVARIANT_MAX = 8,
	ALE_INVARIANT_MIN = 16,
	ALE_INVARIANT_MEDIAN = 32
};

/*
 * Status return values.
 *
 * Based loosely on OpenCL status return values.
 */

enum {
	ALE_UNSPECIFIED_FAILURE = -1,
	ALE_SUCCESS = 0
};

/*
 * Image buffer types.
 */

enum {
	ALE_BUFFER_CL,
	ALE_BUFFER_HOST,
	ALE_BUFFER_FILE
};

/*
 * Obtain a Libale context given an OpenCL context 
 */

ale_context ale_new_context(cl_context cc);

/*
 * Retain an OpenCL context from a Libale context.
 */

cl_context ale_context_retain_cl(ale_context ac);

/*
 * Set a context property.
 */

int ale_context_set_property(ale_context ac, const char *name, const char *value);

/*
 * Set a context-wide signal recipient
 */

int ale_context_set_receiver(ale_context ac, void (*receiver_callback)(const char *signal, va_list ap));

/*
 * Send a context signal
 */

int ale_context_vsignal(ale_context ac, const char *signal, va_list ap);
int ale_context_signal(ale_context ac, const char *signal, ...);

/*
 * Context preferred coordinate resolution.
 */

ALE_API_PARAMETER_RW(ale_context, coords_type, int)

/*
 * Context preferred color resolution.
 */

ALE_API_PARAMETER_RW(ale_context, color_type, int)

/*
 * Context preferred certainty resolution.
 */

ALE_API_PARAMETER_RW(ale_context, certainty_type, int)

/*
 * Context preferred weight resolution.
 */

ALE_API_PARAMETER_RW(ale_context, weight_type, int)

/*
 * Obtain a Libale transformation
 */

ale_trans ale_new_trans(ale_context ac, ale_image ai);

/*
 * Set original bounds for a Libale transformation.
 */

int ale_trans_set_original_bounds(ale_trans at, ale_image ai);

/*
 * Rescale a transform with a given factor.
 */

int ale_trans_rescale(ale_trans at, double factor);

/*
 * Transformation exposure parameter.
 */

ALE_API_PARAMETER_RW(ale_trans, exposure, double)

/*
 * Transformation gamma parameter.  (Default is 0.45.)
 */

ALE_API_PARAMETER_RW(ale_trans, gamma, double)

/*
 * Transformation black level parameter, expressed as a fraction of saturation.
 */

ALE_API_PARAMETER_RW(ale_trans, black, double)

/*
 * Transformation bayer parameter.
 */

ALE_API_PARAMETER_RW(ale_trans, bayer, int)

/*
 * Transformation certainty curve
 */

ALE_API_RC_PARAMETER_RW(ale_trans, certainty, ale_certainty)

/*
 * Create a Libale image.
 */

ale_image ale_new_image(ale_context ac, int format, int type);

/*
 * Convenience function to clone a Libale image (matching size and format, but
 * not copying contents).
 */

ale_image ale_clone_image(ale_image source, int output_type);

/*
 * Convenience function to scale a Libale image, producing a new (unit gamma,
 * non-bayer) image.
 */

ale_image ale_scale_image(ale_image source, double factor, int output_type, double source_gamma, int source_bayer);
ale_image ale_scale_image_wt(ale_image source, double factor, int output_type, int source_bayer);

/*
 * Set a Libale image to a given OpenCL buffer, host buffer, or file buffer.
 */

int ale_image_set_cl(ale_image ai, size_t width, size_t height, cl_mem buffer);
int ale_image_set_host_dynamic(ale_image ai, size_t width, size_t height, void *buffer);
int ale_image_set_host_static(ale_image ai, size_t width, size_t height, void *buffer, void (*release_callback)(void *), void *callback_data);
int ale_image_set_file_dynamic(ale_image ai, size_t width, size_t height, FILE *buffer);
int ale_image_set_file_static(ale_image ai, size_t width, size_t height, FILE *buffer, long offset, int (*release_callback)(void *), void *callback_data);

/*
 * Buffer type for a given Libale image.
 */

ALE_API_PARAMETER_R(ale_image, buffer_type, int)

/*
 * Retain an OpenCL buffer, host buffer, or file buffer, given a Libale image.
 * (returns NULL or ((cl_mem) 0) on failure).
 */

cl_mem ale_image_retain_cl(ale_image ai);
void *ale_image_retain_host(ale_image ai);
FILE *ale_image_retain_file(ale_image ai);

/*
 * Release data without internal reference counters.
 */

int ale_image_release_host(ale_image ai, void *hi);
int ale_image_release_file(ale_image ai, FILE *fi);

/*
 * Resize a Libale image. 
 */

int ale_resize_image(ale_image ai, int x_offset, int y_offset, size_t width, size_t height);

/*
 * Get image statistics.
 */

ALE_API_PARAMETER_R(ale_image, height, size_t)
ALE_API_PARAMETER_R(ale_image, width, size_t)
ALE_API_PARAMETER_R(ale_image, format, int)
ALE_API_PARAMETER_R(ale_image, type, int)
ALE_API_PARAMETER_RW(ale_image, x_offset, int)
ALE_API_PARAMETER_RW(ale_image, y_offset, int)

size_t ale_image_get_depth(ale_image ai);

/*
 * XXX: It is possible that minval and maxval would be better implemented
 * outside of the API, using other, more generic calls (such as eval).
 */

double ale_image_maxval(ale_image source);
double ale_image_minval(ale_image source);
ale_image ale_image_nn_fill(ale_image source, double nn_defined_radius);
ale_image ale_image_get_weights(ale_image source);

/*
 * Evaluation operator.
 */

int ale_eval(const char *s, ...);

#if 0

/*
 * Proposed function evaluation operator, detecting types from a given named
 * function.
 */

int ale_eval_func(const char *s, ...);

#endif

/*
 * Create a Libale incremental renderer, with given invariant.
 */

ale_render ale_new_render_incremental(ale_context ac, int type, int invariant);

/*
 * Libale renderer definition threshold parameter
 */

ALE_API_PARAMETER_RW(ale_render, definition_threshold, double)

/*
 * Libale renderer saturation threshold parameter
 */

ALE_API_PARAMETER_RW(ale_render, saturation_threshold, double)

/*
 * Libale renderer maximum frame range parameter
 */

ALE_API_PARAMETER_RW(ale_render, range_threshold, double)

/*
 * Render resize.
 */

int ale_render_resize(ale_render ar, ale_trans at);

/*
 * Render extend.
 */

 int ale_render_extend(ale_render ar, ale_trans at);

/*
 * Render merge.
 */

int ale_render_merge(ale_render ar, ale_trans at, ale_image ai);

/*
 * Render retain image.
 */

ale_image ale_render_retain_image(ale_render ar);

/*
 * Synchronized renderer resize.
 */

int ale_render_resize_sync(ale_render ar, ale_sequence s, int i);

/*
 * Synchronized render extend.
 */

int ale_render_extend_sync(ale_render ar, ale_sequence s, int i);

/*
 * Synchronized renderer merge.
 */

int ale_render_merge_sync(ale_render ar, ale_sequence s, int i);

/*
 * Create a Libale Irani-Peleg renderer, with given reference.
 */

ale_render ale_new_render_ip(ale_context ac, ale_render reference, int iterations);

/*
 * Create a renderer ensemble.
 */

ale_render ale_new_render_ensemble(ale_context ac);

/*
 * Retain a renderer from an ensemble.
 */

ale_render ale_render_ensemble_retain(ale_render ar, const char *spec);

/*
 * Perform any stream processing (e.g., Irani-Peleg) run.
 */

int ale_render_stream_process(ale_render ar, ale_sequence s);

/*
 * Retain an image from a sequence
 */

ale_image ale_sequence_retain_image(ale_sequence s, int i);

/*
 * Release an image from a sequence
 */

int ale_sequence_release_image(ale_sequence, int i);

/*
 * Create a filter.
 */

ale_filter ale_new_filter(ale_context ac, const char *type);

/*
 * Create a point-spread function.
 */

ale_psf ale_new_psf(ale_context ac, const char *type);

/*
 * Create an exclusion region list.
 */

ale_exclusion_list ale_new_exclusion_list(ale_context ac);

/*
 * Append an exclusion region to a list
 */

int ale_exclusion_list_append(ale_exclusion_list ae, int is_frame_coords, double xmin, double xmax, double ymin, double ymax, double fmin, double fmax);

/*
 * Obtain an alignment RESULT between frame A and current approximation B,
 * starting from transformation START, and given alignment properties
 * ALIGN_PROPERTIES.
 */

int ale_align(ale_context ac, ale_image a, ale_image b, ale_trans start,
		ale_align_properties align_properties, ale_trans result);


/*
 * Create an alignment properties structure.
 */

ale_align_properties ale_new_align_properties();

/*
 * Return a PSF error based on a captured sequence and a scene approximation.
 */

double ale_psf_error(ale_image approx, ale_sequence sequence);



#endif
