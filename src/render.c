/*  
 *  Copyright 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libale.h"

/*
 * API types.
 */

TYPE(ale_render, \
	ale_context ac; \
	ale_image render_data; \
	double definition_threshold; \
	double saturation_threshold; \
	double range_threshold;, \
	ale_image_release(P(this)->render_data);)

/*
 * API Implementation.
 */

ale_render ale_new_render_incremental(ale_context ac, int type, int invariant) {
	if (!ale_context_valid(ac))
		return N(ale_render);

	if (type != ALE_TYPE_FLOAT_32 && type != ALE_TYPE_FLOAT_64)
		return N(ale_render);

	ale_image render_data = ale_new_image(ac, ALE_IMAGE_WEIGHTED_RGB, type);

	if (!ale_image_valid(render_data))
		return N(ale_render);

	ale_render ar = ale_render_alloc(ac);

	if (!ale_render_valid(ar)) {
		ale_image_release(render_data);
		return N(ale_render);
	}

	Q(ale_render, ar)->ac = ac;
	Q(ale_render, ar)->render_data = render_data;

	return ar;
}

int ale_render_resize(ale_render ar, ale_trans at) {
#warning function unimplemented
	return 0;
}

int ale_render_merge(ale_render ar, ale_trans at, ale_image ai) {
#warning function unimplemented
	return 0;
}

ale_image ale_render_retain_image(ale_render ar) {
	if (!ale_render_valid(ar))
		return N(ale_image);

	if (!Q(ale_render, ar))
		return N(ale_image);

	ale_image ai = Q(ale_render, ar)->render_data;

	ale_image_retain(ai);

	return ai;
}

PARAMETER_RW(ale_render, definition_threshold, double)
PARAMETER_RW(ale_render, saturation_threshold, double)
PARAMETER_RW(ale_render, range_threshold, double)
