/*  
 *  Copyright 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libale.h"

/*
 * API types.
 */

TYPE(ale_trans, \
	size_t height; \
	size_t width; \
	ale_context ac; \
	double exposure; \
	double black; \
	double gamma; \
	int bayer;, \
	)

/*
 * API Implementation.
 */

ale_trans ale_new_trans(ale_context ac, ale_image ai) {
#warning NULL image argument should produce the (Euclidean) identity (e.g., for a 2x2 image, as in legacy ALE).
	if (!ale_context_valid(ac))
		return N(ale_trans);

	if (!ale_image_valid(ai))
		return N(ale_trans);

	ale_trans at = ale_trans_alloc(ac);

	if (!ale_trans_valid(at))
		return N(ale_trans);

	DQ(ale_trans, at, atp)

	atp->width = ale_image_get_width(ai);
	atp->height = ale_image_get_height(ai);
	atp->exposure = 1.0;
	atp->gamma = 0.45;
	atp->black = 0.0;
	atp->ac = ac;

	return at;
}

/*
 * Set original bounds for a Libale transformation.
 */

int ale_trans_set_original_bounds(ale_trans at, ale_image ai) {
#warning raw imported code should be revised for Libale
#if 0
/*
 * Original ALE header copyright notice gives '2002, 2004, 2007 David Hilvert'
 * for the following, but git-blame (correctly) gives 2008.
 */
        /*              
         * Set the bounds of the reference image after incorporation
         * of the original frame.
         */     
        void set_original_bounds(const image *i) {
                assert (orig_ref_width == 0);
                assert (orig_ref_height == 0);
                
                orig_ref_height = i->height();
                orig_ref_width = i->width();
                orig_offset = i->offset();
                
                assert (orig_ref_width != 0);
                assert (orig_ref_height != 0);
        }
                
#endif

	return ALE_SUCCESS;
}


PARAMETER_RW(ale_trans, bayer, int)
PARAMETER_RW(ale_trans, exposure, double)
PARAMETER_RW(ale_trans, gamma, double)
PARAMETER_RW(ale_trans, black, double)

