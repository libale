/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libale.h"

/*
 * API types.
 */

TYPE(ale_exclusion_list, \
	, \
	)

/*
 * API Implementation.
 */

ale_exclusion_list ale_new_exclusion_list(ale_context ac) {
#warning function unfinished.
}

int ale_exclusion_list_append(ale_exclusion_list ae, int is_frame_coords, double xmin, double xmax, double ymin, double ymax, double fmin, double fmax) {
#warning function unfinished.
}

