/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ale.h>

#include <math.h>
#include <stdlib.h>

/*
 * For now, do nothing with assertions.  This is useful for preserving
 * assertion statements used in code imported from ALE, and may be useful for
 * emitting debugging messages in the future.
 */

#define ale_assert(x) ;

/*
 * Abstract types used internally by host code (cf. ale_context_get_*_type for
 * preferred computational types).
 */

typedef double ale_pos;
typedef double ale_accum;
typedef double ale_real;

typedef struct { ale_pos x[3]; } point;

static point point2(ale_pos x0, ale_pos x1) {
	point p;

	p.x[0] = x0;
	p.x[1] = x1;
	p.x[2] = 0;
}

static point point3(ale_pos x0, ale_pos x1, ale_pos x2) {
	point p;

	p.x[0] = x0;
	p.x[1] = x1;
	p.x[2] = x2;
}

static point point_posinf(int dim) {
	int d;
	point p;

	/* Division logic from ALE d2::point::posinf */

	ale_pos a = +1;
	ale_pos z = +0;

	a = a / z;

	for (d = 0; d < 3; d++)
		p.x[d] = (d < dim) ? a : 0;

	return p;
}

static point point_neginf(int dim) {
	int d;
	point p = point_posinf(dim);

	for (d = 0; d < dim && d < 3; d++)
		p.x[d] = -p.x[d];

	return p;
}

static int point_defined2(point p) {
	return (!isnan(p.x[0]) && !isnan(p.x[1]));
}

static int point_defined3(point p) {
	return (!isnan(p.x[0]) && !isnan(p.x[1]) && !isnan(p.x[2]));
}

/*
 * Element of the device memory table.
 *
 * XXX: this might be better done as a table with struct { ale_context c; cl_mem m[0]; };
 */

typedef union {
	cl_mem memory;
	ale_context context;
} mem_table_entry;

/*
 * Libale void and file pointer types.
 */

ALE_POINTER(void, libale_voidp)
ALE_POINTER(FILE, libale_filep)

/*
 * Get ale_context associated with a table index from an ALE_POINTER to device
 * memory.
 */

static ale_context api_to_ac(mem_table_entry *p) {

	/*
	 * Get the slot just prior to an invalid cl_mem (marking the start of
	 * the cl_mem table.
	 */

	while (p->memory != ((cl_mem) 0))
		p--;

	p--;

	return p->context;
}

/*
 * Macro for obtaining a pointer from ALE_POINTER type.
 */

#define P(x) x.p

/*
 * Macro for converting (typecasting) index to mem_table_entry.
 */

#define LIBALE_INDEX_TO_MEMENTRY(x) ((mem_table_entry *) (((char *) NULL) + x.t.index))

/*
 * Macro for the reverse (but with bare index).
 */

#define LIBALE_MEMENTRY_TO_INDEX(x) ((cl_long) (((char *) x) - ((char *) NULL)))

/*
 * Macro for obtaining an ale_context associated with an ALE_POINTER to device
 * memory.
 */

#define C(x) api_to_ac(LIBALE_INDEX_TO_MEMENTRY(x))

/*
 * Macro for obtaining a pointer to host-mapped memory from an ALE_POINTER
 * type to device memory.  We assume Q idempotent over multiple invocations.
 * We also assume results from Q stale after a kernel execution.
 */

#define Q(TYPENAME, x) ((struct _ ## TYPENAME *) ale_context_map(C(x), LIBALE_INDEX_TO_MEMENTRY(x)))

/*
 * Macro for creating a variable pointer to host-mapped memory via Q().
 */

#define DQ(TYPENAME, x, px) struct _ ## TYPENAME *px = Q(TYPENAME, x);

/*
 * Context interface for mapping, idempotent over multiple invocations.
 */

void *ale_context_map(ale_context ac, mem_table_entry *index);

/*
 * Macro for unmapping a previously mapped ALE_POINTER with P(p) = Q(x).  (We
 * assume that calling this is generally unnecessary, and that the context will
 * unmap automatically when necessary, such as prior to a kernel execution or 
 * [is this necessary?] prior to freeing of a mementry argument.)
 */

#define R(x, p) ale_context_unmap(C(x), LIBALE_INDEX_TO_MEMENTRY(x), P(p))

/*
 * Context interface for allocating, referencing, and freeing memory table
 * elements.
 */

mem_table_entry *ale_context_malloc(ale_context ac, size_t s);
mem_table_entry *ale_context_mref(ale_context ac, cl_mem m);
cl_mem ale_context_mretain(ale_context ac, mem_table_entry *e);
void ale_context_mrelease(ale_context ac, mem_table_entry *e);

/*
 * Null macro.
 */

#define N(TYPENAME) TYPENAME ## _NULL()

/*
 * Context interface for unmapping.
 */

int ale_context_unmap(ale_context ac, mem_table_entry *index, void *p);

/*
 * Macros for common API type elements and functions.
 */

/*
 * We do our own reference counting, both because the CL spec states that
 * returned reference counts are unsuitable for general uses (although they
 * would probably be usable here, allowing destruction functionality, by
 * incrementing reference count by one at the time of object creation), and
 * also because using our own structure member allows us to reference count in
 * host memory, or in a sub-allocated portion of a cl_mem object (allowing the
 * possibility of several Libale objects per cl_mem allocation).
 */

#define TYPE_COMMON_ELEMENTS \
	unsigned int refcount;

#define TYPE_STRUCTURE(TYPENAME, TYPE_ELEMENTS) \
	struct _ ## TYPENAME { \
		TYPE_COMMON_ELEMENTS \
		TYPE_ELEMENTS \
	};

#define TYPE_HOST_COMMON_FUNCTIONS(TYPENAME, DESTRUCTOR) \
	int TYPENAME ## _valid(TYPENAME xarg) { \
		if (P(xarg) == NULL || P(xarg)->refcount == 0) \
			return 0; \
		return 1; \
	} \
	\
	TYPENAME TYPENAME ## _NULL() { \
		TYPENAME result; \
		P(result) = NULL; \
		return result; \
	} \
	\
	int TYPENAME ## _retain(TYPENAME xarg) { \
		if (!TYPENAME ## _valid(xarg)) \
			return ALE_UNSPECIFIED_FAILURE; \
		if (!(P(xarg)->refcount + 1 > 0)) \
			return ALE_UNSPECIFIED_FAILURE; \
		P(xarg)->refcount++; \
		return ALE_SUCCESS; \
	} \
	\
	static TYPENAME TYPENAME ## _alloc() { \
		TYPENAME retval; \
		P(retval) = (struct _ ## TYPENAME *) malloc(sizeof(struct _ ## TYPENAME)); \
		if (P(retval)) \
			P(retval)->refcount = 1; \
		return retval; \
	} \
	\
	static void TYPENAME ## _free(TYPENAME this) { \
		DESTRUCTOR \
	} \
	\
	int TYPENAME ## _release(TYPENAME xarg) { \
		if (!TYPENAME ## _valid(xarg)) \
			return ALE_UNSPECIFIED_FAILURE; \
		P(xarg)->refcount--; \
		if (P(xarg)->refcount == 0)  {\
			TYPENAME ## _free(xarg); \
			free(P(xarg)); \
		} \
		return ALE_SUCCESS; \
	}

#define TYPE_COMMON_FUNCTIONS(TYPENAME, DESTRUCTOR) \
	int TYPENAME ## _valid(TYPENAME xarg) { \
		if (xarg.t.index == 0 || Q(TYPENAME, xarg) == NULL || Q(TYPENAME, xarg)->refcount == 0) \
			return 0; \
		return 1; \
	} \
	\
	TYPENAME TYPENAME ## _NULL() { \
		TYPENAME result; \
		result.t.index = 0; \
		return result; \
	} \
	\
	int TYPENAME ## _retain(TYPENAME xarg) { \
		if (!TYPENAME ## _valid(xarg)) \
			return ALE_UNSPECIFIED_FAILURE; \
		if (!(Q(TYPENAME, xarg)->refcount + 1 > 0)) \
			return ALE_UNSPECIFIED_FAILURE; \
		Q(TYPENAME, xarg)->refcount++; \
		return ALE_SUCCESS; \
	} \
	\
	static TYPENAME TYPENAME ## _alloc(ale_context ac) { \
		TYPENAME retval; \
		mem_table_entry *e = ale_context_malloc(ac, sizeof(struct _ ## TYPENAME)); \
		retval.t.index = LIBALE_MEMENTRY_TO_INDEX(e); \
		if (retval.t.index == 0) \
			return retval; \
		if (!Q(TYPENAME, retval)) { \
			ale_context_mrelease(ac, e); \
			return TYPENAME ## _NULL(); \
		} \
		Q(TYPENAME, retval)->refcount = 1; \
		return retval; \
	} \
	\
	static void TYPENAME ## _free(TYPENAME this) { \
		DESTRUCTOR \
	} \
	\
	int TYPENAME ## _release(TYPENAME xarg) { \
		if (!TYPENAME ## _valid(xarg)) \
			return ALE_UNSPECIFIED_FAILURE; \
		Q(TYPENAME, xarg)->refcount--; \
		if (Q(TYPENAME, xarg)->refcount == 0)  {\
			TYPENAME ## _free(xarg); \
			ale_context_mrelease(C(xarg), LIBALE_INDEX_TO_MEMENTRY(xarg)); \
		} \
		return ALE_SUCCESS; \
	}

#define TYPE_HOST(TYPENAME, TYPE_ELEMENTS, DESTRUCTOR) \
	TYPE_STRUCTURE(TYPENAME, TYPE_ELEMENTS) \
	TYPE_HOST_COMMON_FUNCTIONS(TYPENAME, DESTRUCTOR)

#define TYPE(TYPENAME, TYPE_ELEMENTS, DESTRUCTOR) \
	ALE_DATA_TYPE(_ ## TYPENAME ## _def, TYPE_STRUCTURE(TYPENAME, TYPE_ELEMENTS) ) \
	TYPE_COMMON_FUNCTIONS(TYPENAME, DESTRUCTOR)

/*
 * Macros for API parameter operations.
 */

#define HOST_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_TYPE OBJECT_TYPE ## _get_ ## PARAMETER_NAME (OBJECT_TYPE object) { \
		if (!OBJECT_TYPE ## _valid(object)) \
			return ((PARAMETER_TYPE) 0); \
		return P(object)->PARAMETER_NAME; \
	}

#define HOST_PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	int OBJECT_TYPE ## _set_ ## PARAMETER_NAME (OBJECT_TYPE object, PARAMETER_TYPE value){ \
		if (!OBJECT_TYPE ## _valid(object)) \
			return ALE_UNSPECIFIED_FAILURE; \
		P(object)->PARAMETER_NAME = value; \
		return ALE_SUCCESS; \
	}

#define HOST_PARAMETER_RW(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	HOST_PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	HOST_PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE)

#define PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_TYPE OBJECT_TYPE ## _get_ ## PARAMETER_NAME (OBJECT_TYPE object) { \
		if (!OBJECT_TYPE ## _valid(object)) \
			return ((PARAMETER_TYPE) 0); \
		return Q(OBJECT_TYPE, object)->PARAMETER_NAME; \
	}

#define PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	int OBJECT_TYPE ## _set_ ## PARAMETER_NAME (OBJECT_TYPE object, PARAMETER_TYPE value){ \
		if (!OBJECT_TYPE ## _valid(object)) \
			return ALE_UNSPECIFIED_FAILURE; \
		Q(OBJECT_TYPE, object)->PARAMETER_NAME = value; \
		return ALE_SUCCESS; \
	}

#define PARAMETER_RW(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_R(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE) \
	PARAMETER_W(OBJECT_TYPE, PARAMETER_NAME, PARAMETER_TYPE)

/*
 * Detemine type size.
 */

static int type_size(int type) {
	if (type == ALE_TYPE_UINT_8)
		return 1;
	if (type == ALE_TYPE_UINT_16)
		return 2;
	if (type == ALE_TYPE_FLOAT_32 || type == ALE_TYPE_UINT_32)
		return 4;
	if (type == ALE_TYPE_FLOAT_64 || type == ALE_TYPE_UINT_64)
		return 8;
}
