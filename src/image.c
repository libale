/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libale.h"

/*
 * Infrastructure.
 */

typedef union {
	libale_voidp mem;
	libale_filep file;
} image_buffer;

/*
 * API types.
 */

TYPE_STRUCTURE(ale_image, \
	size_t height; \
	size_t width; \
	int type; \
	int format; \
	ale_context ac; \
	int buffer_type; \
	int buffer_static; \
	image_buffer buffer;)

static void release_memory(ale_image ai) {

	DQ(ale_image, ai, qai)

	if (qai->buffer_type == ALE_BUFFER_CL)
		ale_context_mrelease(qai->ac, LIBALE_INDEX_TO_MEMENTRY(qai->buffer.mem));
	else if (qai->buffer_type == ALE_BUFFER_HOST && !qai->buffer_static)
		free(P(qai->buffer.mem));
	else if (qai->buffer_type == ALE_BUFFER_FILE && !qai->buffer_static)
		fclose(P(qai->buffer.file));

	qai->buffer_type = ALE_BUFFER_HOST;
	P(qai->buffer.mem) = NULL;
}

TYPE_COMMON_FUNCTIONS(ale_image, \
	release_memory(this);)

/*
 * API Implementation.
 */

ale_image ale_new_image(ale_context ac, int format, int type) {
	if (!ale_context_valid(ac))
		return N(ale_image);

	if (type != ALE_TYPE_FLOAT_32 && type != ALE_TYPE_FLOAT_64)
		return N(ale_image);

	ale_image ai = ale_image_alloc(ac);

	if (!ale_image_valid(ai))
		return N(ale_image);

	DQ(ale_image, ai, qai)

	qai->type = type;
	qai->format = format;
	qai->width = 0;
	qai->height = 0;
	qai->ac = ac;
	qai->buffer_type = ALE_BUFFER_HOST;
	P(qai->buffer.mem) = NULL;

	return ai;
}

int ale_resize_image(ale_image ai, int x_offset, int y_offset, size_t width, size_t height) {
#warning function unfinished.
}

int ale_image_set_cl(ale_image ai, size_t width, size_t height, cl_mem buffer) {
	if (!ale_image_valid(ai))
		return ALE_UNSPECIFIED_FAILURE;
	
	if (buffer == ((cl_mem) 0))
		return ALE_UNSPECIFIED_FAILURE;

	release_memory(ai);

	DQ(ale_image, ai, qai)

	qai->buffer_type = ALE_BUFFER_CL;
	qai->buffer.mem.t.index = LIBALE_MEMENTRY_TO_INDEX(ale_context_mref(qai->ac, buffer));
	qai->width = width;
	qai->height = height;

	if (qai->buffer.mem.t.index == 0)
		return ALE_UNSPECIFIED_FAILURE;

	return ALE_SUCCESS;
}

int ale_image_set_host_dynamic(ale_image ai, size_t width, size_t height, void *buffer) {
#warning unimplemented function
	return ALE_UNSPECIFIED_FAILURE;
}

int ale_image_set_host_static(ale_image ai, size_t width, size_t height, void *buffer, void (*release_callback)(void *), void *callback_data) {
#warning unimplemented function
	return ALE_UNSPECIFIED_FAILURE;
}

PARAMETER_R(ale_image, buffer_type, int)

cl_mem ale_image_retain_cl(ale_image ai) {
	if (!ale_image_valid(ai) || Q(ale_image, ai)->buffer_type != ALE_BUFFER_CL)
		return ((cl_mem) 0);

	DQ(ale_image, ai, qai)

	return ale_context_mretain(qai->ac, LIBALE_INDEX_TO_MEMENTRY(qai->buffer.mem));
}

void *ale_image_retain_host(ale_image ai) {
#warning function unimplemented
	return NULL;
}

FILE *ale_image_retain_file(ale_image ai) {
#warning function unimplemented
	return NULL;
}

int ale_image_release_host(ale_image ai, void *hi) {
#warning function unimplemented
	return 0;
}

int ale_image_release_file(ale_image ai, FILE *fi) {
#warning function unimplemented
	return 0;
}

ale_image ale_image_nn_fill(ale_image ai, double defined_radius) {
#warning raw imported code should be revised for libale

#if 0
/*
 * Original ALE header copyright notice gives '2002 David Hilvert' for the
 * following, but git-blame (correctly) gives 2005.
 */
                                /*
                                 * Get nearest-neighbor defined values.
                                 *
                                 * XXX: While this implementation is correct, it is inefficient
                                 * for large radii.  A better implementation would search
                                 * perimeters of squares of ever-increasing radius, tracking
                                 * the best-so-far data until the square perimeter exceeded the
                                 * best-so-far radius.
                                 */

                                for (int k = 0; k < 3; k++)
                                if (isnan(value[k]))
                                for (int radius = 1; radius <= nn_defined_radius; radius++) {
                                        double nearest_radius_squared = (radius + 1) * (radius + 1);
                                        for (int ii = -radius; ii <= radius; ii++)
                                        for (int jj = -radius; jj <= radius; jj++) {
                                                if (!im->in_bounds(point(i + ii, j + jj)))
                                                        continue;
                                                if (ii * ii + jj * jj < nearest_radius_squared
                                                 && finite(im->get_pixel(i + ii, j + jj)[k])) {
                                                        value[k] = im->get_pixel(i + ii, j + jj)[k];
                                                        nearest_radius_squared = ii * ii + jj * jj;
                                                }
                                        }
                                        if (nearest_radius_squared < (radius + 1) * (radius + 1))
                                                break;
                                }

                                /*
                                 * Unlinearize
                                 */

                                pixel unlinearized(exp->unlinearize((value - minval_pixel)
                                                                  / (maxval - minval)));

                                unlinearized = unlinearized.clamp();
#endif
}

ale_image ale_image_get_weights(ale_image ai) {
#warning function unimplemented
	return N(ale_image);
}

PARAMETER_R(ale_image, height, size_t)
PARAMETER_R(ale_image, width, size_t)
PARAMETER_R(ale_image, type, int)
PARAMETER_R(ale_image, format, int)
