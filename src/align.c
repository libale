/*  
 *  Copyright 2002, 2004, 2005, 2006, 2007, 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>

#include "libale.h"

/*
 * API Implementation.
 */

/*
 * Types for scale clusters.
 */

#if 0

/*
 * Experimental non-linear scaling approach.
 */

struct nl_scale_cluster {
	ale_image accum_max;
	ale_image accum_min;
	ale_image aweight_max;
	ale_image aweight_min;

	ale_pos input_scale;
	ale_image input_certainty_max;
	ale_image input_certainty_min;
	ale_image input_max;
	ale_image input_min;
};
#endif

struct scale_cluster {
	ale_image accum;
	ale_image aweight;

	ale_pos input_scale;
	ale_image input_certainty;
	ale_image input;

#if 0
	nl_scale_cluster *nl_scale_clusters;
#endif
};


static struct scale_cluster *init_clusters(ale_context ac, int frame, ale_pos scale_factor,
		ale_image input_frame, ale_trans input_trans, ale_image reference_image, 
		ale_image alignment_weights, unsigned int steps, ale_filter interpolant) {

	int input_bayer = ale_trans_get_bayer(input_trans);
	ale_real input_gamma = ale_trans_get_gamma(input_trans);
	ale_certainty input_certainty = ale_trans_retain_certainty(input_trans);
	ale_real input_black = ale_trans_get_black(input_trans);

	unsigned int step;

	/*
	 * Allocate memory for the array.
	 */

	struct scale_cluster *scale_clusters = 
		(struct scale_cluster *) malloc(steps * sizeof(struct scale_cluster));

	if (!scale_clusters)
		return NULL;

	/*
	 * Prepare images and exclusion regions for the highest level
	 * of detail.  
	 */

	scale_clusters[0].accum = reference_image;

	ale_context_signal(ac, "ui_constructing_lod_clusters", 0.0);

	scale_clusters[0].input_scale = scale_factor;
	if (scale_factor < 1.0 && !ale_filter_valid(interpolant))
		scale_clusters[0].input = ale_scale_image(input_frame, scale_factor, ale_context_get_color_type(ac), input_gamma, input_bayer);
	else
		scale_clusters[0].input = input_frame;

	scale_clusters[0].aweight = alignment_weights;

	/*
	 * Allocate and determine input frame certainty.
	 */

	scale_clusters[0].input_certainty = ale_clone_image(scale_clusters[0].input, ale_context_get_certainty_type(ac));

	ale_eval("MAP_PIXEL(%0I, p, CERTAINTY(%1t, p, %2f, GET_AFFINE_PIXEL(%3i, p, %4f)))",
			scale_clusters[0].input_certainty, input_black, input_certainty,
			scale_clusters[0].input, (scale_factor < 1.0 && !ale_filter_valid(interpolant)) ? 1 : input_gamma);

#if 0
	init_nl_cluster(&(scale_clusters[0]));
#endif

	/*
	 * Prepare reduced-detail images and exclusion
	 * regions.
	 */

	for (step = 1; step < steps; step++) {
		ale_context_signal(ac, "ui_constructing_lod_clusters", step);
		scale_clusters[step].accum = ale_scale_image(scale_clusters[step - 1].accum, 0.5, ale_context_get_color_type(ac), 1, ALE_BAYER_NONE);
		scale_clusters[step].aweight = ale_scale_image_wt(scale_clusters[step - 1].aweight, 0.5, ale_context_get_weight_type(ac), ALE_BAYER_NONE);

		ale_pos sf = scale_clusters[step - 1].input_scale;

		scale_clusters[step].input_scale = sf / 2;

		if (sf >= 2.0 || ale_filter_valid(interpolant)) {
			scale_clusters[step].input = scale_clusters[step - 1].input;
			scale_clusters[step].input_certainty = scale_clusters[step - 1].input_certainty;
		} else if (sf >= 1.0) {
			scale_clusters[step].input = ale_scale_image(scale_clusters[step - 1].input, sf / 2, ale_context_get_color_type(ac), input_gamma, input_bayer);
			scale_clusters[step].input_certainty = ale_scale_image_wt(scale_clusters[step - 1].input_certainty, sf / 2, ale_context_get_certainty_type(ac), input_bayer);
		} else {
			scale_clusters[step].input = ale_scale_image(scale_clusters[step - 1].input, 0.5, ale_context_get_color_type(ac), 1, ALE_BAYER_NONE);
			scale_clusters[step].input_certainty = ale_scale_image_wt(scale_clusters[step - 1].input_certainty, 0.5, ale_context_get_certainty_type(ac), ALE_BAYER_NONE);
		}

#if 0
		init_nl_cluster(&(scale_clusters[step]));
#endif
	}

	ale_certainty_release(input_certainty);

	return scale_clusters;
}

/*
 * Not-quite-symmetric difference function.  Determines the difference in areas
 * where the arrays overlap.  Uses the reference array's notion of pixel positions.
 *
 * For the purposes of determining the difference, this function divides each
 * pixel value by the corresponding image's average pixel magnitude, unless we
 * are:
 *
 * a) Extending the boundaries of the image, or
 * 
 * b) following the previous frame's transform
 *
 * If we are doing monte-carlo pixel sampling for alignment, we
 * typically sample a subset of available pixels; otherwise, we sample
 * all pixels.
 *
 */

ALE_API_TYPEDEF(ale_align_run)

TYPE(ale_align_run, 
	ale_trans offset; \
	ale_accum result; \
	ale_accum divisor; \
	point max; \
	point min; \
	ale_accum centroid[2]; \
	ale_accum centroid_divisor; \
	ale_accum de_centroid[2]; \
	ale_accum de_centroid_v; 
	ale_accum de_sum;, \
	)

typedef ale_align_run run;

static void run_init(run r, ale_trans offset) {

	DQ(ale_align_run, r, qr)

	qr->result = 0;
	qr->divisor = 0;

	qr->min = point_posinf(2);
	qr->max = point_neginf(2);

	qr->centroid[0] = 0;
	qr->centroid[1] = 0;
	qr->centroid_divisor = 0;

	qr->de_centroid[0] = 0;
	qr->de_centroid[1] = 0;

	qr->de_centroid_v = 0;

	qr->de_sum = 0;

	qr->offset = offset;
}

static void run_add(run r, const run s) {
	int d;

	DQ(ale_align_run, r, qr)
	DQ(ale_align_run, s, qs)

	qr->result += qs->result;
	qr->divisor += qs->divisor;

	for (d = 0; d < 2; d++) {
		if (qr->min.x[d] > qs->min.x[d])
			qr->min.x[d] = qs->min.x[d];
		if (qr->max.x[d] < qs->max.x[d])
			qr->max.x[d] = qs->max.x[d];
		qr->centroid[d] += qs->centroid[d];
		qr->de_centroid[d] += qs->de_centroid[d];
	}

	qr->centroid_divisor += qs->centroid_divisor;
	qr->de_centroid_v += qs->de_centroid_v;
	qr->de_sum += qs->de_sum;
}

static ale_accum run_get_error(run r, ale_real metric_exponent) {

	DQ(ale_align_run, r, qr)

	return pow(qr->result / qr->divisor, 1/(ale_accum) metric_exponent);
}

#warning the following should probably be converted to vectorized code.
#if 0
static void run_sample(int f, struct scale_cluster c, int i, int j, point t, point u) {

	pixel pa = c.accum->get_pixel(i, j);

	ale_real this_result[2] = { 0, 0 };
	ale_real this_divisor[2] = { 0, 0 };

	pixel p[2];
	pixel weight[2];
	weight[0] = pixel(1, 1, 1);
	weight[1] = pixel(1, 1, 1);

	pixel tm = offset.get_tonal_multiplier(point(i, j) + c.accum->offset());

	if (interpolant != NULL) {
		interpolant->filtered(i, j, &p[0], &weight[0], 1, f);

		if (weight[0].min_norm() > ale_real_weight_floor) {
			p[0] /= weight[0];
		} else {
			return;
		}

	} else {
		p[0] = c.input->get_bl(t);
	}

	p[0] *= tm;

	if (u.defined()) {
		p[1] = c.input->get_bl(u);
		p[1] *= tm;
	}


	/*
	 * Handle certainty.
	 */

	if (certainty_weights == 1) {

		/*
		 * For speed, use arithmetic interpolation (get_bl(.))
		 * instead of geometric (get_bl(., 1))
		 */

		weight[0] *= c.input_certainty->get_bl(t);
		if (u.defined())
			weight[1] *= c.input_certainty->get_bl(u);
		weight[0] *= c.certainty->get_pixel(i, j);
		weight[1] *= c.certainty->get_pixel(i, j);
	}

	if (c.aweight != NULL) {
		weight[0] *= c.aweight->get_pixel(i, j);
		weight[1] *= c.aweight->get_pixel(i, j);
	}

	/*
	 * Update sampling area statistics
	 */

	if (min[0] > i)
		min[0] = i;
	if (min[1] > j)
		min[1] = j;
	if (max[0] < i)
		max[0] = i;
	if (max[1] < j)
		max[1] = j;

	centroid[0] += (weight[0][0] + weight[0][1] + weight[0][2]) * i;
	centroid[1] += (weight[0][0] + weight[0][1] + weight[0][2]) * j;
	centroid_divisor += (weight[0][0] + weight[0][1] + weight[0][2]);

	/*
	 * Determine alignment type.
	 */

	for (int m = 0; m < (u.defined() ? 2 : 1); m++)
	if (channel_alignment_type == 0) {
		/*
		 * Align based on all channels.
		 */


		for (int k = 0; k < 3; k++) {
			ale_real achan = pa[k];
			ale_real bchan = p[m][k];

			this_result[m] += weight[m][k] * pow(fabs(achan - bchan), metric_exponent);
			this_divisor[m] += weight[m][k] * pow(achan > bchan ? achan : bchan, metric_exponent);
		}
	} else if (channel_alignment_type == 1) {
		/*
		 * Align based on the green channel.
		 */

		ale_real achan = pa[1];
		ale_real bchan = p[m][1];

		this_result[m] = weight[m][1] * pow(fabs(achan - bchan), metric_exponent);
		this_divisor[m] = weight[m][1] * pow(achan > bchan ? achan : bchan, metric_exponent);
	} else if (channel_alignment_type == 2) {
		/*
		 * Align based on the sum of all channels.
		 */

		ale_real asum = 0;
		ale_real bsum = 0;
		ale_real wsum = 0;

		for (int k = 0; k < 3; k++) {
			asum += pa[k];
			bsum += p[m][k];
			wsum += weight[m][k] / 3;
		}

		this_result[m] = wsum * pow(fabs(asum - bsum), metric_exponent);
		this_divisor[m] = wsum * pow(asum > bsum ? asum : bsum, metric_exponent);
	}

	if (u.defined()) {
//					ale_real de = fabs(this_result[0] / this_divisor[0]
//						         - this_result[1] / this_divisor[1]);
		ale_real de = fabs(this_result[0] - this_result[1]);

		de_centroid[0] += de * (ale_real) i;
		de_centroid[1] += de * (ale_real) j;

		de_centroid_v += de * (ale_real) t.lengthto(u);

		de_sum += de;
	}

	result += (this_result[0]);
	divisor += (this_divisor[0]);
}
#endif

static void run_rescale(run r, ale_pos scale) {
	DQ(ale_align_run, r, qr)

	ale_trans_rescale(qr->offset, scale);

	qr->de_centroid[0] *= scale;
	qr->de_centroid[1] *= scale;
	qr->de_centroid_v *= scale;
}

static point run_get_centroid(run r) {
	DQ(ale_align_run, r, qr)

	point result = point2(qr->centroid[0] / qr->centroid_divisor, qr->centroid[1] / qr->centroid_divisor);

	ale_assert (finite(qr->centroid[0]) 
	     && finite(qr->centroid[1]) 
	     && (result.defined() || centroid_divisor == 0));

	return result;
}

static point run_get_error_centroid(run r) {
	DQ(ale_align_run, r, qr)

	point result = point2(qr->de_centroid[0] / qr->de_sum, qr->de_centroid[1] / qr->de_sum);
	return result;
}


static ale_pos run_get_error_perturb(run r) {
	DQ(ale_align_run, r, qr)

	ale_pos result = qr->de_centroid_v / qr->de_sum;

	return result;
}

template<class diff_trans>
class diff_stat_generic {

	transformation::elem_bounds_t elem_bounds;

	/*
	 * When non-empty, runs.front() is best, runs.back() is
	 * testing.
	 */

	std::vector<run> runs;

	/*
	 * old_runs stores the latest available perturbation set for
	 * each multi-alignment element.
	 */

	typedef int run_index;
	std::map<run_index, run> old_runs;

	static void *diff_subdomain(void *args);

	struct subdomain_args {
		struct scale_cluster c;
		std::vector<run> runs;
		int ax_count;
		int f;
		int i_min, i_max, j_min, j_max;
		int subdomain;
	};

	struct scale_cluster si;
	int ax_count;
	int frame;

	std::vector<ale_pos> perturb_multipliers;

public:
	void diff(struct scale_cluster c, const diff_trans &t, 
			int _ax_count, int f) {

		if (runs.size() == 2)
			runs.pop_back();

		runs.push_back(run(t));

		si = c;
		ax_count = _ax_count;
		frame = f;

		ui::get()->d2_align_sample_start();

		if (interpolant != NULL) {

			/*
			 * XXX: This has not been tested, and may be completely
			 * wrong.
			 */

			transformation tt = transformation::eu_identity();
			tt.set_current_element(t);
			interpolant->set_parameters(tt, c.input, c.accum->offset());
		}

		int N;
#ifdef USE_PTHREAD
		N = thread::count();

		pthread_t *threads = (pthread_t *) malloc(sizeof(pthread_t) * N);
		pthread_attr_t *thread_attr = (pthread_attr_t *) malloc(sizeof(pthread_attr_t) * N);

#else
		N = 1;
#endif

		subdomain_args *args = new subdomain_args[N];

		transformation::elem_bounds_int_t b = elem_bounds.scale_to_bounds(c.accum->height(), c.accum->width());

// 			fprintf(stdout, "[%d %d] [%d %d]\n", 
// 				global_i_min, global_i_max, global_j_min, global_j_max);

		for (int ti = 0; ti < N; ti++) {
			args[ti].c = c;
			args[ti].runs = runs;
			args[ti].ax_count = ax_count;
			args[ti].f = f;
			args[ti].i_min = b.imin + ((b.imax - b.imin) * ti) / N;
			args[ti].i_max = b.imin + ((b.imax - b.imin) * (ti + 1)) / N;
			args[ti].j_min = b.jmin;
			args[ti].j_max = b.jmax;
			args[ti].subdomain = ti;
#ifdef USE_PTHREAD
			pthread_attr_init(&thread_attr[ti]);
			pthread_attr_setdetachstate(&thread_attr[ti], PTHREAD_CREATE_JOINABLE);
			pthread_create(&threads[ti], &thread_attr[ti], diff_subdomain, &args[ti]);
#else
			diff_subdomain(&args[ti]);
#endif
		}

		for (int ti = 0; ti < N; ti++) {
#ifdef USE_PTHREAD
			pthread_join(threads[ti], NULL);
#endif
			runs.back().add(args[ti].runs.back());
		}

#ifdef USE_PTHREAD
		free(threads);
		free(thread_attr);
#endif

		delete[] args;

		ui::get()->d2_align_sample_stop();

	}

private:
	void rediff() {
		std::vector<diff_trans> t_array;

		for (unsigned int r = 0; r < runs.size(); r++) {
			t_array.push_back(runs[r].offset);
		}

		runs.clear();

		for (unsigned int r = 0; r < t_array.size(); r++)
			diff(si, t_array[r], ax_count, frame);
	}


public:
	int better() {
		assert(runs.size() >= 2);
		assert(runs[0].offset.scale() == runs[1].offset.scale());

		return (runs[1].get_error() < runs[0].get_error() 
		     || (!finite(runs[0].get_error()) && finite(runs[1].get_error())));
	}

	int better_defined() {
		assert(runs.size() >= 2);
		assert(runs[0].offset.scale() == runs[1].offset.scale());

		return (runs[1].get_error() < runs[0].get_error()); 
	}

	diff_stat_generic(transformation::elem_bounds_t e) 
			: runs(), old_runs(), perturb_multipliers() {
		elem_bounds = e;
	}

	run_index get_run_index(unsigned int perturb_index) {
		return perturb_index;
	}

	run &get_run(unsigned int perturb_index) {
		run_index index = get_run_index(perturb_index);

		assert(old_runs.count(index));
		return old_runs[index];
	}

	void rescale(ale_pos scale, scale_cluster _si) {
		assert(runs.size() == 1);

		si = _si;

		runs[0].rescale(scale);
		
		rediff();
	}

	~diff_stat_generic() {
	}

	diff_stat_generic &operator=(const diff_stat_generic &dst) {
		/*
		 * Copy run information.
		 */
		runs = dst.runs;
		old_runs = dst.old_runs;

		/*
		 * Copy diff variables
		 */
		si = dst.si;
		ax_count = dst.ax_count;
		frame = dst.frame;
		perturb_multipliers = dst.perturb_multipliers;
		elem_bounds = dst.elem_bounds;

		return *this;
	}

	diff_stat_generic(const diff_stat_generic &dst) : runs(), old_runs(),
					      perturb_multipliers() {
		operator=(dst);
	}

	void set_elem_bounds(transformation::elem_bounds_t e) {
		elem_bounds = e;
	}

	ale_accum get_result() {
		assert(runs.size() == 1);
		return runs[0].result;
	}

	ale_accum get_divisor() {
		assert(runs.size() == 1);
		return runs[0].divisor;
	}

	diff_trans get_offset() {
		assert(runs.size() == 1);
		return runs[0].offset;
	}

	int operator!=(diff_stat_generic &param) {
		return (get_error() != param.get_error());
	}

	int operator==(diff_stat_generic &param) {
		return !(operator!=(param));
	}

	ale_pos get_error_perturb() {
		assert(runs.size() == 1);
		return runs[0].get_error_perturb();
	}

	ale_accum get_error() const {
		assert(runs.size() == 1);
		return runs[0].get_error();
	}

public:
	/*
	 * Get the set of transformations produced by a given perturbation
	 */
	void get_perturb_set(std::vector<diff_trans> *set, 
			ale_pos adj_p, ale_pos adj_o, ale_pos adj_b, 
			ale_pos *current_bd, ale_pos *modified_bd,
			std::vector<ale_pos> multipliers = std::vector<ale_pos>()) {

		assert(runs.size() == 1);

		diff_trans test_t(diff_trans::eu_identity());

		/* 
		 * Translational or euclidean transformation
		 */

		for (unsigned int i = 0; i < 2; i++)
		for (unsigned int s = 0; s < 2; s++) {

			if (!multipliers.size())
				multipliers.push_back(1);

			assert(finite(multipliers[0]));

			test_t = get_offset();

			// test_t.eu_modify(i, (s ? -adj_p : adj_p) * multipliers[0]);
			test_t.translate((i ? point(1, 0) : point(0, 1))
				       * (s ? -adj_p : adj_p)
				       * multipliers[0]);
			
			test_t.snap(adj_p / 2);

			set->push_back(test_t);
			multipliers.erase(multipliers.begin());

		}

		if (alignment_class > 0)
		for (unsigned int s = 0; s < 2; s++) {

			if (!multipliers.size())
				multipliers.push_back(1);

			assert(multipliers.size());
			assert(finite(multipliers[0]));

			if (!(adj_o * multipliers[0] < rot_max))
				return;

			ale_pos adj_s = (s ? 1 : -1) * adj_o * multipliers[0];

			test_t = get_offset();

			run_index ori = get_run_index(set->size());
			point centroid = point::undefined();

			if (!old_runs.count(ori))
				ori = get_run_index(0);

			if (!centroid.finite() && old_runs.count(ori)) {
				centroid = old_runs[ori].get_error_centroid();

				if (!centroid.finite())
					centroid = old_runs[ori].get_centroid();

				centroid *= test_t.scale() 
					  / old_runs[ori].offset.scale();
			}

			if (!centroid.finite() && !test_t.is_projective()) {
				test_t.eu_modify(2, adj_s);
			} else if (!centroid.finite()) {
				centroid = point(si.input->height() / 2, 
						 si.input->width() / 2);

				test_t.rotate(centroid + si.accum->offset(),
						adj_s);
			} else {
				test_t.rotate(centroid + si.accum->offset(), 
						adj_s);
			}

			test_t.snap(adj_p / 2);

			set->push_back(test_t);
			multipliers.erase(multipliers.begin());
		}

		if (alignment_class == 2) {

			/*
			 * Projective transformation
			 */

			for (unsigned int i = 0; i < 4; i++)
			for (unsigned int j = 0; j < 2; j++)
			for (unsigned int s = 0; s < 2; s++) {

				if (!multipliers.size())
					multipliers.push_back(1);

				assert(multipliers.size());
				assert(finite(multipliers[0]));

				ale_pos adj_s = (s ? -1 : 1) * adj_p * multipliers [0];

				test_t = get_offset();

				if (perturb_type == 0)
					test_t.gpt_modify_bounded(j, i, adj_s, elem_bounds.scale_to_bounds(si.accum->height(), si.accum->width()));
				else if (perturb_type == 1)
					test_t.gr_modify(j, i, adj_s);
				else
					assert(0);

				test_t.snap(adj_p / 2);

				set->push_back(test_t);
				multipliers.erase(multipliers.begin());
			}

		}

		/*
		 * Barrel distortion
		 */

		if (bda_mult != 0 && adj_b != 0) {

			for (unsigned int d = 0; d < get_offset().bd_count(); d++)
			for (unsigned int s = 0; s < 2; s++) {

				if (!multipliers.size())
					multipliers.push_back(1);

				assert (multipliers.size());
				assert (finite(multipliers[0]));

				ale_pos adj_s = (s ? -1 : 1) * adj_b * multipliers[0];

				if (bda_rate > 0 && fabs(modified_bd[d] + adj_s - current_bd[d]) > bda_rate)
					continue;
			
				diff_trans test_t = get_offset();

				test_t.bd_modify(d, adj_s);

				set->push_back(test_t);
			}
		}
	}

	void confirm() {
		assert(runs.size() == 2);
		runs[0] = runs[1];
		runs.pop_back();
	}

	void discard() {
		assert(runs.size() == 2);
		runs.pop_back();
	}

	void perturb_test(ale_pos perturb, ale_pos adj_p, ale_pos adj_o, ale_pos adj_b, 
			ale_pos *current_bd, ale_pos *modified_bd, int stable) {

		assert(runs.size() == 1);

		std::vector<diff_trans> t_set;

		if (perturb_multipliers.size() == 0) {
			get_perturb_set(&t_set, adj_p, adj_o, adj_b, 
					current_bd, modified_bd);

			for (unsigned int i = 0; i < t_set.size(); i++) {
				diff_stat_generic test = *this;

				test.diff(si, t_set[i], ax_count, frame);

				test.confirm();

				if (finite(test.get_error_perturb()))
					perturb_multipliers.push_back(adj_p / test.get_error_perturb());
				else
					perturb_multipliers.push_back(1);

			}

			t_set.clear();
		}

		get_perturb_set(&t_set, adj_p, adj_o, adj_b, current_bd, modified_bd,
				perturb_multipliers);

		int found_unreliable = 1;
		std::vector<int> tested(t_set.size(), 0);

		for (unsigned int i = 0; i < t_set.size(); i++) {
			run_index ori = get_run_index(i);

			/*
			 * Check for stability
			 */
			if (stable
			 && old_runs.count(ori)
			 && old_runs[ori].offset == t_set[i])
				tested[i] = 1;
		}

		std::vector<ale_pos> perturb_multipliers_original = perturb_multipliers;

		while (found_unreliable) {

			found_unreliable = 0;

			for (unsigned int i = 0; i < t_set.size(); i++) {

				if (tested[i])
					continue;

				diff(si, t_set[i], ax_count, frame);

				if (!(i < perturb_multipliers.size())
				 || !finite(perturb_multipliers[i])) {

					perturb_multipliers.resize(i + 1);

					if (finite(perturb_multipliers[i])
					 && finite(adj_p)
					 && finite(adj_p / runs[1].get_error_perturb())) {
						perturb_multipliers[i] = 
							adj_p / runs[1].get_error_perturb();

						found_unreliable = 1;
					} else
						perturb_multipliers[i] = 1;

					continue;
				}

				run_index ori = get_run_index(i);

				if (old_runs.count(ori) == 0)
					old_runs.insert(std::pair<run_index, run>(ori, runs[1]));
				else 
					old_runs[ori] = runs[1];

				if (finite(perturb_multipliers_original[i])
				 && finite(runs[1].get_error_perturb())
				 && finite(adj_p)
				 && finite(perturb_multipliers_original[i] * adj_p / runs[1].get_error_perturb()))
					perturb_multipliers[i] = perturb_multipliers_original[i]
						* adj_p / runs[1].get_error_perturb();
				else
					perturb_multipliers[i] = 1;

				tested[i] = 1;

				if (better()
				 && runs[1].get_error() < runs[0].get_error()
				 && perturb_multipliers[i] 
				  / perturb_multipliers_original[i] < 2) {
					runs[0] = runs[1];
					runs.pop_back();
					return;
				}

			}

			if (runs.size() > 1)
				runs.pop_back();

			if (!found_unreliable)
				return;
		}
	}

};

template<class diff_trans>
void *d2::align::diff_stat_generic<diff_trans>::diff_subdomain(void *args) {

	subdomain_args *sargs = (subdomain_args *) args;

	struct scale_cluster c = sargs->c;
	std::vector<run> runs = sargs->runs;
	int ax_count = sargs->ax_count;
	int f = sargs->f;
	int i_min = sargs->i_min;
	int i_max = sargs->i_max;
	int j_min = sargs->j_min;
	int j_max = sargs->j_max;
	int subdomain = sargs->subdomain;

	assert (reference_image);

	point offset = c.accum->offset();

	for (mc_iterate m(i_min, i_max, j_min, j_max, subdomain); !m.done(); m++) {

		int i = m.get_i();
		int j = m.get_j();

		/*
		 * Check reference frame definition.
		 */

		if (!((pixel) c.accum->get_pixel(i, j)).finite()
		 || !(((pixel) c.certainty->get_pixel(i, j)).minabs_norm() > 0))
			continue;

		/*
		 * Check for exclusion in render coordinates.
		 */

		if (ref_excluded(i, j, offset, c.ax_parameters, ax_count))
			continue;

		/*
		 * Transform
		 */

		struct point q, r = point::undefined();

		q = (c.input_scale < 1.0 && interpolant == NULL)
		  ? runs.back().offset.scaled_inverse_transform(
			point(i + offset[0], j + offset[1]))
		  : runs.back().offset.unscaled_inverse_transform(
			point(i + offset[0], j + offset[1]));

		if (runs.size() == 2) {
			r = (c.input_scale < 1.0)
			  ? runs.front().offset.scaled_inverse_transform(
				point(i + offset[0], j + offset[1]))
			  : runs.front().offset.unscaled_inverse_transform(
				point(i + offset[0], j + offset[1]));
		}

		ale_pos ti = q[0];
		ale_pos tj = q[1];

		/*
		 * Check that the transformed coordinates are within
		 * the boundaries of array c.input and that they
		 * are not subject to exclusion.
		 *
		 * Also, check that the weight value in the accumulated array
		 * is nonzero, unless we know it is nonzero by virtue of the
		 * fact that it falls within the region of the original frame
		 * (e.g. when we're not increasing image extents).
		 */

		if (input_excluded(ti, tj, c.ax_parameters, ax_count))
			continue;

		if (input_excluded(r[0], r[1], c.ax_parameters, ax_count))
			r = point::undefined();

		/*
		 * Check the boundaries of the input frame
		 */

		if (!(ti >= 0
		   && ti <= c.input->height() - 1
		   && tj >= 0
		   && tj <= c.input->width() - 1))
			continue;

		if (!(r[0] >= 0
		   && r[0] <= c.input->height() - 1
		   && r[1] >= 0
		   && r[1] <= c.input->width() - 1))
			r = point::undefined();

		sargs->runs.back().sample(f, c, i, j, q, r, runs.front());
	}

	return NULL;
}

typedef diff_stat_generic<trans_single> diff_stat_t;
typedef diff_stat_generic<trans_multi> diff_stat_multi;


/*
 * Adjust exposure for an aligned frame B against reference A.
 *
 * Expects full-LOD images.
 *
 * Note: This method does not use any weighting, by certainty or
 * otherwise, in the first exposure registration pass, as any bias of
 * weighting according to color may also bias the exposure registration
 * result; it does use weighting, including weighting by certainty
 * (even if certainty weighting is not specified), in the second pass,
 * under the assumption that weighting by certainty improves handling
 * of out-of-range highlights, and that bias of exposure measurements
 * according to color may generally be less harmful after spatial
 * registration has been performed.
 */
class exposure_ratio_iterate : public thread::decompose_domain {
	pixel_accum *asums;
	pixel_accum *bsums;
	pixel_accum *asum;
	pixel_accum *bsum;
	struct scale_cluster c;
	const transformation &t;
	int ax_count;
	int pass_number;
protected:
	void prepare_subdomains(unsigned int N) {
		asums = new pixel_accum[N];
		bsums = new pixel_accum[N];
	}
	void subdomain_algorithm(unsigned int thread,
			int i_min, int i_max, int j_min, int j_max) {

		point offset = c.accum->offset();

		for (mc_iterate m(i_min, i_max, j_min, j_max, thread); !m.done(); m++) {
			
			unsigned int i = (unsigned int) m.get_i();
			unsigned int j = (unsigned int) m.get_j();

			if (ref_excluded(i, j, offset, c.ax_parameters, ax_count))
				continue;

			/*
			 * Transform
			 */

			struct point q;

			q = (c.input_scale < 1.0 && interpolant == NULL)
			  ? t.scaled_inverse_transform(
				point(i + offset[0], j + offset[1]))
			  : t.unscaled_inverse_transform(
				point(i + offset[0], j + offset[1]));

			/*
			 * Check that the transformed coordinates are within
			 * the boundaries of array c.input, that they are not
			 * subject to exclusion, and that the weight value in
			 * the accumulated array is nonzero.
			 */

			if (input_excluded(q[0], q[1], c.ax_parameters, ax_count))
				continue;

			if (q[0] >= 0
			 && q[0] <= c.input->height() - 1
			 && q[1] >= 0
			 && q[1] <= c.input->width() - 1
			 && ((pixel) c.certainty->get_pixel(i, j)).minabs_norm() != 0) { 
				pixel a = c.accum->get_pixel(i, j);
				pixel b;

				b = c.input->get_bl(q);

				pixel weight = ((c.aweight && pass_number)
					      ? (pixel) c.aweight->get_pixel(i, j)
					      : pixel(1, 1, 1))
					     * (pass_number
					      ? psqrt(c.certainty->get_pixel(i, j)
						    * c.input_certainty->get_bl(q, 1))
					      : pixel(1, 1, 1));

				asums[thread] += a * weight;
				bsums[thread] += b * weight;
			}
		}
	}

	void finish_subdomains(unsigned int N) {
		for (unsigned int n = 0; n < N; n++) {
			*asum += asums[n];
			*bsum += bsums[n];
		}
		delete[] asums;
		delete[] bsums;
	}
public:
	exposure_ratio_iterate(pixel_accum *_asum,
			       pixel_accum *_bsum,
			       struct scale_cluster _c,
			       const transformation &_t,
			       int _ax_count,
			       int _pass_number) : decompose_domain(0, _c.accum->height(), 
								    0, _c.accum->width()),
						   t(_t) {

		asum = _asum;
		bsum = _bsum;
		c = _c;
		ax_count = _ax_count;
		pass_number = _pass_number;
	}

	exposure_ratio_iterate(pixel_accum *_asum,
			       pixel_accum *_bsum,
			       struct scale_cluster _c,
			       const transformation &_t,
			       int _ax_count,
			       int _pass_number,
			       transformation::elem_bounds_int_t b) : decompose_domain(b.imin, b.imax, 
								       b.jmin, b.jmax),
						      t(_t) {

		asum = _asum;
		bsum = _bsum;
		c = _c;
		ax_count = _ax_count;
		pass_number = _pass_number;
	}
};

static void set_exposure_ratio(unsigned int m, struct scale_cluster c,
		const transformation &t, int ax_count, int pass_number) {

	if (_exp_register == 2) {
		/*
		 * Use metadata only.
		 */
		ale_real gain_multiplier = image_rw::exp(m).get_gain_multiplier();
		pixel multiplier = pixel(gain_multiplier, gain_multiplier, gain_multiplier);

		image_rw::exp(m).set_multiplier(multiplier);
		ui::get()->exp_multiplier(multiplier[0],
					  multiplier[1],
					  multiplier[2]);

		return;
	}

	pixel_accum asum(0, 0, 0), bsum(0, 0, 0);

	exposure_ratio_iterate eri(&asum, &bsum, c, t, ax_count, pass_number);
	eri.run();

	// std::cerr << (asum / bsum) << " ";
	
	pixel_accum new_multiplier;

	new_multiplier = asum / bsum * image_rw::exp(m).get_multiplier();

	if (finite(new_multiplier[0])
	 && finite(new_multiplier[1])
	 && finite(new_multiplier[2])) {
		image_rw::exp(m).set_multiplier(new_multiplier);
		ui::get()->exp_multiplier(new_multiplier[0],
					  new_multiplier[1],
					  new_multiplier[2]);
	}
}

static diff_stat_t _align_element(ale_pos perturb, ale_pos local_lower,
		scale_cluster *scale_clusters, diff_stat_t here,
		ale_pos adj_p, ale_pos adj_o, ale_pos adj_b, 
		ale_pos *current_bd, ale_pos *modified_bd,
		astate_t *astate, int lod, scale_cluster si) {

	/*
	 * Run initial tests to get perturbation multipliers and error
	 * centroids.
	 */

	std::vector<d2::trans_single> t_set;

	here.get_perturb_set(&t_set, adj_p, adj_o, adj_b, current_bd, modified_bd);

	int stable_count = 0;

	while (perturb >= local_lower) {
		
		ui::get()->alignment_dims(scale_clusters[lod].accum->height(), scale_clusters[lod].accum->width(),
					  scale_clusters[lod].input->height(), scale_clusters[lod].input->width());

		/*
		 * Orientational adjustment value in degrees.
		 *
		 * Since rotational perturbation is now specified as an
		 * arclength, we have to convert.
		 */

		ale_pos adj_o = 2 * (double) perturb 
				  / sqrt(pow(scale_clusters[0].input->height(), 2)
				       + pow(scale_clusters[0].input->width(),  2))
				  * 180
				  / M_PI;

		/*
		 * Barrel distortion adjustment value
		 */

		ale_pos adj_b = perturb * bda_mult;

		trans_single old_offset = here.get_offset();

		here.perturb_test(perturb, adj_p, adj_o, adj_b, current_bd, modified_bd,
			stable_count);

		if (here.get_offset() == old_offset)
			stable_count++;
		else
			stable_count = 0;

		if (stable_count == 3) {

			stable_count = 0;

			perturb *= 0.5;

			if (lod > 0 
			 && lod > lrint(log(perturb) / log(2)) - lod_preferred) {

				/*
				 * Work with images twice as large
				 */

				lod--;
				si = scale_clusters[lod];

				/* 
				 * Rescale the transforms.
				 */

				ale_pos rescale_factor = (double) scale_factor
						       / (double) pow(2, lod)
						       / (double) here.get_offset().scale();

				here.rescale(rescale_factor, si);

			} else {
				adj_p = perturb / pow(2, lod);
			}

			/*
			 * Announce changes
			 */

			ui::get()->alignment_perturbation_level(perturb, lod);
		}

		ui::get()->set_match(here.get_error());
		ui::get()->set_offset(here.get_offset());
	}

	if (lod > 0) {
		ale_pos rescale_factor = (double) scale_factor
				       / (double) here.get_offset().scale();

		here.rescale(rescale_factor, scale_clusters[0]);
	}

	return here;
}

/*
 * Align frame m against the reference.
 *
 * XXX: the transformation class currently combines ordinary
 * transformations with scaling.  This is somewhat convenient for
 * some things, but can also be confusing.  This method, _align(), is
 * one case where special care must be taken to ensure that the scale
 * is always set correctly (by using the 'rescale' method).
 */
static diff_stat_multi _align(int m, int local_gs, astate_t *astate) {

	ale_image input_frame = astate->get_input_frame();

	/*
	 * Local upper/lower data, possibly dependent on image
	 * dimensions.
	 */

	ale_pos local_lower, local_upper;
	ale_accum local_gs_mo;

	/*
	 * Select the minimum dimension as the reference.
	 */

	ale_pos reference_size = input_frame->height();
	if (input_frame->width() < reference_size)
		reference_size = input_frame->width();
	ale_accum reference_area = input_frame->height()
				 * input_frame->width();

	if (perturb_lower_percent)
		local_lower = (double) perturb_lower
			    * (double) reference_size
			    * (double) 0.01
			    * (double) scale_factor;
	else
		local_lower = perturb_lower;

	if (perturb_upper_percent)
		local_upper = (double) perturb_upper
			    * (double) reference_size
			    * (double) 0.01
			    * (double) scale_factor;
	else
		local_upper = perturb_upper;

	local_upper = pow(2, floor(log(local_upper) / log(2)));

	if (gs_mo_percent)
		local_gs_mo = (double) _gs_mo
			    * (double) reference_area
			    * (double) 0.01
			    * (double) scale_factor;
	else
		local_gs_mo = _gs_mo;

	/*
	 * Logarithms aren't exact, so we divide repeatedly to discover
	 * how many steps will occur, and pass this information to the
	 * user interface.
	 */

	int step_count = 0;
	double step_variable = local_upper;
	while (step_variable >= local_lower) {
		step_variable /= 2;
		step_count++;
	}

	ale_pos perturb = local_upper;

	if (_keep) {
		kept_t[latest] = latest_t;
		kept_ok[latest] = latest_ok;
	}

	/*
	 * Determine how many levels of detail should be prepared, by
	 * calculating the initial (largest) value for the
	 * level-of-detail variable.
	 */

	int lod = lrint(log(perturb) / log(2)) - lod_preferred;

	if (lod < 0)
		lod = 0;

	while (lod > 0 && (reference_image->width() < pow(2, lod) * min_dimension
			|| reference_image->height() < pow(2, lod) * min_dimension))
		lod--;

	unsigned int steps = (unsigned int) lod + 1;

	/*
	 * Prepare multiple levels of detail.
	 */

	int local_ax_count;
	struct scale_cluster *scale_clusters = init_clusters(m,
			scale_factor, input_frame, steps,
			&local_ax_count);

#error add check for non-NULL return

	/*
	 * Initialize the default initial transform
	 */

	astate->init_default();

	/*
	 * Set the default transformation.
	 */

	transformation offset = astate->get_default();

	/*
	 * Establish boundaries
	 */

	offset.set_current_bounds(reference_image);

	ui::get()->alignment_degree_max(offset.get_coordinate(offset.stack_depth() - 1).degree);

	if (offset.stack_depth() == 1) {
		ui::get()->set_steps(step_count, 0);
	} else {
		ui::get()->set_steps(offset.get_coordinate(offset.stack_depth() - 1).degree + 1, 1);
	}

	/*
	 * Load any file-specified transformations
	 */

	for (unsigned int index = 0; index < offset.stack_depth(); index++) {
		int is_default = 1;
		unsigned int index_2;
		offset.set_current_index(index);

		offset = tload_next(tload, alignment_class == 2, 
				offset, 
				&is_default, offset.get_current_index() == 0);

		index_2 = offset.get_current_index();

		if (index_2 > index) {
			for (unsigned int index_3 = index; index_3 < index_2; index_3++)
				astate->set_is_default(index_3, 1);

			index = index_2;
		}

		astate->set_is_default(index, is_default);
	}

	offset.set_current_index(0);

	astate->init_frame_alignment_primary(&offset, lod, perturb);

	/*
	 * Control point alignment
	 */

	if (local_gs == 5) {

		transformation o = offset;

		/*
		 * Determine centroid data
		 */

		point current, previous;
		centroids(m, &current, &previous);

		if (current.defined() && previous.defined()) {
			o = orig_t;
			o.set_dimensions(input_frame);
			o.translate((previous - current) * o.scale());
			current = previous;
		}

		/*
		 * Determine rotation for alignment classes other than translation.
		 */

		ale_pos lowest_error = cp_rms_error(m, o);

		ale_pos rot_lower = 2 * (double) local_lower
				  / sqrt(pow(scale_clusters[0].input->height(), 2)
				       + pow(scale_clusters[0].input->width(),  2))
				  * 180
				  / M_PI;

		if  (alignment_class > 0)
		for (double rot = 30; rot > rot_lower; rot /= 2) 
		for (double srot = -rot; srot < rot * 1.5; srot += rot * 2) {
			int is_improved = 1;
			while (is_improved) {
				is_improved = 0;
				transformation test_t = o;
				/*
				 * XXX: is this right?
				 */
				test_t.rotate(current * o.scale(), srot);
				ale_pos test_v = cp_rms_error(m, test_t);

				if (test_v < lowest_error) {
					lowest_error = test_v;
					o = test_t;
					srot += 3 * rot;
					is_improved = 1;
				}
			}
		}

		/*
		 * Determine projective parameters through a local
		 * minimum search.
		 */

		if (alignment_class == 2) {
			ale_pos adj_p = lowest_error;

			if (adj_p < local_lower)
				adj_p = local_lower;

			while (adj_p >= local_lower) {
				transformation test_t = o;
				int is_improved = 1;
				ale_pos test_v;
				ale_pos adj_s;

				while (is_improved) {
					is_improved = 0;

					for (int i = 0; i < 4; i++)
					for (int j = 0; j < 2; j++)
					for (adj_s = -adj_p; adj_s <= adj_p; adj_s += 2 * adj_p) {

						test_t = o;

						if (perturb_type == 0)
							test_t.gpt_modify(j, i, adj_s);
						else if (perturb_type == 1)
							test_t.gr_modify(j, i, adj_s);
						else
							assert(0);

						test_v = cp_rms_error(m, test_t);

						if (test_v < lowest_error) {
							lowest_error = test_v;
							o = test_t;
							adj_s += 3 * adj_p;
							is_improved = 1;
						}
					}
				}
				adj_p /= 2;
			}
		}
	}

	/*
	 * Pre-alignment exposure adjustment
	 */

	if (_exp_register) {
		ui::get()->exposure_1();
		set_exposure_ratio(m, scale_clusters[0], offset, local_ax_count, 0);
	}

	/*
	 * Scale transform for lod
	 */

	for (int lod_ = 0; lod_ < lod; lod_++) {
		transformation s = offset;
		transformation t = offset;

		t.rescale(1 / (double) 2);

		if (!(t.scaled_height() > 0 && t.scaled_height() < s.scaled_height())
		 || !(t.scaled_width() > 0  && t.scaled_width() < s.scaled_width())) {
			perturb /= pow(2, lod - lod_);
			lod = lod_;
			break;
		} else {
			offset = t;
		}
	}

	ui::get()->set_offset(offset);

	struct scale_cluster si = scale_clusters[lod];

	/*
	 * Projective adjustment value
	 */

	ale_pos adj_p = perturb / pow(2, lod);

	/*
	 * Orientational adjustment value in degrees.
	 *
	 * Since rotational perturbation is now specified as an
	 * arclength, we have to convert.
	 */

	ale_pos adj_o = (double) 2 * (double) perturb 
			  / sqrt(pow((double) scale_clusters[0].input->height(), (double) 2)
			       + pow((double) scale_clusters[0].input->width(),  (double) 2))
			  * (double) 180
			  / M_PI;

	/*
	 * Barrel distortion adjustment value
	 */

	ale_pos adj_b = perturb * bda_mult;

	/*
	 * Global search overlap requirements.
	 */

	local_gs_mo = (double) local_gs_mo / pow(pow(2, lod), 2);

	/*
	 * Alignment statistics.
	 */

	diff_stat_t here(offset.elem_bounds());

	/*
	 * Current difference (error) value
	 */

	ui::get()->prematching();
	here.diff(si, offset.get_current_element(), local_ax_count, m);
	ui::get()->set_match(here.get_error());

	/*
	 * Current and modified barrel distortion parameters
	 */

	ale_pos current_bd[BARREL_DEGREE];
	ale_pos modified_bd[BARREL_DEGREE];
	offset.bd_get(current_bd);
	offset.bd_get(modified_bd);

	/*
	 * Translational global search step
	 */

	if (perturb >= local_lower && local_gs != 0 && local_gs != 5
	 && (local_gs != 6 || astate->get_is_default(0))) {
		
		ui::get()->global_alignment(perturb, lod);
		ui::get()->gs_mo(local_gs_mo);

		test_globals(&here, si, offset, local_gs, adj_p,
				local_ax_count, m, local_gs_mo, perturb);

		ui::get()->set_match(here.get_error());
		ui::get()->set_offset(here.get_offset());
	}

	/*
	 * Perturbation adjustment loop.  
	 */

	offset.set_current_element(here.get_offset());

	for (unsigned int i = 0; i < offset.stack_depth(); i++) {

		ui::get()->aligning_element(i, offset.stack_depth());
	
		offset.set_current_index(i);

		ui::get()->start_multi_alignment_element(offset);

		ui::get()->set_offset(offset);

		if (i > 0) {
			astate->init_frame_alignment_nonprimary(&offset, lod, perturb, i);

			if (_exp_register == 1) {
				ui::get()->exposure_1();
				pixel_accum asum(0, 0, 0), bsum(0, 0, 0);
				exposure_ratio_iterate eri(&asum, &bsum, scale_clusters[0], offset, local_ax_count, 0, 
					offset.elem_bounds().scale_to_bounds(scale_clusters[0].accum->height(),
									     scale_clusters[0].accum->width()));

				eri.run();
				pixel_accum tr = asum / bsum;
				ui::get()->exp_multiplier(tr[0], tr[1], tr[2]);
				offset.set_tonal_multiplier(tr);
			}
		}

		int e_lod = lod;
		int e_div = offset.get_current_coordinate().degree;
		ale_pos e_perturb = perturb;
		ale_pos e_adj_p = adj_p;
		ale_pos e_adj_b = adj_b;

		for (int d = 0; d < e_div; d++) {
			e_adj_b = 0;
			e_perturb *= 0.5;
			if (e_lod > 0) {
				e_lod--;
			} else {
				e_adj_p *= 0.5;
			}
		}

		if (i > 0) {

			d2::trans_multi::elem_bounds_t b = offset.elem_bounds();

			for (int dim_satisfied = 0; e_lod > 0 && !dim_satisfied; ) {
				int height = scale_clusters[e_lod].accum->height();
				int width = scale_clusters[e_lod].accum->width();

				d2::trans_multi::elem_bounds_int_t bi = b.scale_to_bounds(height, width);

				dim_satisfied = bi.satisfies_min_dim(min_dimension);
				
				if (!dim_satisfied) {
					e_lod--;
					e_adj_p *= 2;
				}
			}

			/*
			 * Scale transform for lod
			 */

			for (int lod_ = 0; lod_ < e_lod; lod_++) {
				trans_single s = offset.get_element(i);
				trans_single t = offset.get_element(i);

				t.rescale(1 / (double) 2);

				if (!(t.scaled_height() > 0 && t.scaled_height() < s.scaled_height())
				 || !(t.scaled_width() > 0  && t.scaled_width() < s.scaled_width())) {
					e_perturb /= pow(2, e_lod - lod_);
					e_lod = lod_;
					break;
				} else {
					offset.set_element(i, t);
				}
			}

			ui::get()->set_offset(offset);
		}

		/*
		 * Announce perturbation size
		 */

		ui::get()->aligning(e_perturb, e_lod);

		si = scale_clusters[e_lod];

		here.set_elem_bounds(offset.elem_bounds());

		here.diff(si, offset.get_current_element(), local_ax_count, m);

		here.confirm();

		here = check_ancestor_path(offset, si, here, local_ax_count, m);

		here = _align_element(e_perturb, local_lower, scale_clusters, 
				here, e_adj_p, adj_o, e_adj_b, current_bd, modified_bd,
				astate, e_lod, si);

		offset.rescale(here.get_offset().scale() / offset.scale());

		offset.set_current_element(here.get_offset());

		if (i > 0 && _exp_register == 1) {
			if (ma_cert_satisfied(scale_clusters[0], offset, i)) {
				ui::get()->exposure_2();
				pixel_accum asum(0, 0, 0), bsum(0, 0, 0);
				exposure_ratio_iterate eri(&asum, &bsum, scale_clusters[0], offset, local_ax_count, 1, 
					offset.elem_bounds().scale_to_bounds(scale_clusters[0].accum->height(),
									     scale_clusters[0].accum->width()));

				eri.run();
				pixel_accum tr = asum / bsum;
				ui::get()->exp_multiplier(tr[0], tr[1], tr[2]);
				offset.set_tonal_multiplier(tr);
			} else {
				offset.set_tonal_multiplier(offset.get_element(offset.parent_index(i)).get_tonal_multiplier(point(0, 0)));
			}
		} else if (_exp_register == 1) {
			ui::get()->exposure_2();
			set_exposure_ratio(m, scale_clusters[0], offset, local_ax_count, 1);
		}

		ui::get()->set_offset(offset);

		if (i + 1 == offset.stack_depth())
			ui::get()->alignment_degree_complete(offset.get_coordinate(i).degree);
		else if (offset.get_coordinate(i).degree != offset.get_coordinate(i + 1).degree)
			ui::get()->alignment_degree_complete(offset.get_coordinate(i + 1).degree);
	}

	offset.set_current_index(0);

	ui::get()->multi();
	offset.set_multi(reference_image, input_frame);

	/*
	 * Recalculate error on whole frame.
	 */

	ui::get()->postmatching();
	diff_stat_generic<transformation> multi_here(offset.elem_bounds());
	multi_here.diff(scale_clusters[0], offset, local_ax_count, m);
	ui::get()->set_match(multi_here.get_error());

	/*
	 * Free the level-of-detail structures
	 */

	final_clusters(scale_clusters, scale_factor, steps);

	/*
	 * Ensure that the match meets the threshold.
	 */

	if (threshold_ok(multi_here.get_error())) {
		/*
		 * Update alignment variables
		 */
		latest_ok = 1;
		astate->set_default(offset);
		astate->set_final(offset);
		ui::get()->alignment_match_ok();
	} else if (local_gs == 4) {

		/*
		 * Align with outer starting points.
		 */

		/*
		 * XXX: This probably isn't exactly the right thing to do,
		 * since variables like old_initial_value have been overwritten.
		 */

		diff_stat_multi nested_result = _align(m, -1, astate);

		if (threshold_ok(nested_result.get_error())) {
			return nested_result;
		} else if (nested_result.get_error() < multi_here.get_error()) {
			multi_here = nested_result;
		}

		if (is_fail_default)
			offset = astate->get_default();

		ui::get()->set_match(multi_here.get_error());
		ui::get()->alignment_no_match();
	
	} else if (local_gs == -1) {
		
		latest_ok = 0;
		latest_t = offset;
		return multi_here;

	} else {
		if (is_fail_default)
			offset = astate->get_default();
		latest_ok = 0;
		ui::get()->alignment_no_match();
	}

	/*
	 * Write the tonal registration multiplier as a comment.
	 */

	pixel trm = image_rw::exp(m).get_multiplier();
	tsave_trm(tsave, trm[0], trm[1], trm[2]);

	/*
	 * Save the transformation information
	 */

	for (unsigned int index = 0; index < offset.stack_depth(); index++) {
		offset.set_current_index(index);

		tsave_next(tsave, offset, alignment_class == 2, 
				  offset.get_current_index() == 0);
	}

	offset.set_current_index(0);

	/*
	 * Update match statistics.
	 */

	match_sum += (1 - multi_here.get_error()) * (ale_accum) 100;
	match_count++;
	latest = m;
	latest_t = offset;

	return multi_here;
}

int ale_align(ale_context ac, ale_image a, ale_image b, ale_trans start,
		ale_align_properties align_properties, ale_trans result) {
#warning function unfinished.
}

