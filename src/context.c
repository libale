/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libale.h"

#include <string.h>

/*
 * API types.
 */

TYPE_HOST(ale_context, \
	cl_context cc; \
	int compute_resident_size; \
	int host_resident_size; \
	int file_resident_size;, \
	clReleaseContext(P(this)->cc);)

/*
 * API Implementation.
 */

ale_context ale_new_context(cl_context cc) {

	if (cc == ((cl_context) 0) || clRetainContext(cc) != CL_SUCCESS)
		return N(ale_context);

	ale_context ac = ale_context_alloc();

	if (!ale_context_valid(ac)) {
		clReleaseContext(cc);
		return N(ale_context);
	}

	P(ac)->cc = cc;
	P(ac)->compute_resident_size = 0;
	P(ac)->host_resident_size = 0;
	P(ac)->file_resident_size = 0;

	return ac;
}

cl_context ale_context_retain_cl(ale_context ac) {
	if (!ale_context_valid(ac))
		return ((cl_context) 0);

	clRetainContext(P(ac)->cc);
	return P(ac)->cc;
}

int ale_context_set_property(ale_context ac, const char *name, const char *value) {
	if (!strcmp(name, "compute_resident_size")) {
		P(ac)->compute_resident_size = atoi(value);
	} else if (!strcmp(name, "host_resident_size")) {
		P(ac)->host_resident_size = atoi(value);
	} else if (!strcmp(name, "file_resident_size")) {
		P(ac)->file_resident_size = atoi(value);
	} else {
		return ALE_UNSPECIFIED_FAILURE;
	}

	return ALE_SUCCESS;
}

