/*  
 *  Copyright 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

#warning **** 
#warning **** OPENCL STUB FUNCTIONS COMPILED
#warning **** 

#include <ale.h>

/*
 * OpenCL stubs.
 *
 * For more information, see:
 *
 * http://www.khronos.org/registry/cl/
 * http://www.khronos.org/registry/cl/api/1.0/cl.h
 * http://www.khronos.org/registry/cl/specs/opencl-1.0.29.pdf
 */

cl_context clCreateContextFromType(cl_context_properties properties, 
		cl_device_type device_type,
		void (*pfn_notify)(const char *errinfo,
				const void *private_info, size_t cb,
				const void *user_data),
		void *user_data,
		cl_int *errcode_ret) {

	return ((cl_context) 0);
}

cl_int clRetainMemObject(cl_mem memobj) {
	return 0;
}

cl_int clReleaseContext(cl_context context) {
	return 0;
}

cl_int clReleaseMemObject(cl_mem memobj) {
	return 0;
}

cl_int clRetainContext(cl_context context) {
	return 0;
}
