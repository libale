/*  
 *  Copyright 2008, 2009 David Hilvert <dhilvert@gmail.com>
 *
 *  This file is part of libale.
 *
 *  libale is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU Affero General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  libale is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with libale.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * beer: a simple client program for libale.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <ale.h>

/*
 * beer_info_printf: Display informational message for format C.
 */

static void beer_info_printf(const char *c, ...) {
	va_list ap;

	fprintf(stderr, "beer: ");

	va_start(ap, c);
	vfprintf(stderr, c, ap);
	va_end(ap);

	fprintf(stderr, "\n");
}

/*
 * beer_error_printf: Display error message for format C.
 */

static void beer_error_printf(const char *c, ...) {
	va_list ap;

	fprintf(stderr, "beer: ");

	va_start(ap, c);
	vfprintf(stderr, c, ap);
	va_end(ap);

	fprintf(stderr, "\n");
	fprintf(stderr, "beer: Exiting.\n");
	exit(1);
}

/*
 * main: Main routine (entry point).
 */

int main(int argc, const char *argv[]) {

	/*
	 * Obtain OpenCL context CC.
	 */

	cl_context cc = clCreateContextFromType(0, CL_DEVICE_TYPE_GPU, NULL, NULL, NULL);

	if (cc == ((cl_context) 0)) {
		beer_error_printf("Failed to create an OpenCL context.");
	}

	/*
	 * Obtain ALE context AC.
	 */

	ale_context ac = ale_new_context(cc);

	if (ac == NULL) {
		beer_error_printf("Failed to create an ALE context.");
	}

	/*
	 * Create sample input frame AI from trivial image array DOMAIN_DATA.
	 */

	unsigned char domain_data[27] =
		{ 0, 0, 0,  0, 0, 0,  0, 0, 0,
		  0, 0, 0,  1, 1, 1,  0, 0, 0,
		  0, 0, 0,  0, 0, 0,  0, 0, 0 };

	ale_image ai = ale_new_image(ac, ALE_IMAGE_RGB, ALE_TYPE_UINT_8);

	ale_image_set_host_static(ai, 3, 3, domain_data, NULL, NULL);

	/*
	 * Create incremental renderer AR.
	 */

	ale_render ar = ale_new_render_incremental(ac, ALE_TYPE_FLOAT_32, ALE_INVARIANT_MEAN);

	/*
	 * Create identity transformation AT with bounds AI.
	 */

	ale_trans at = ale_new_trans(ac, ai);

	/*
	 * Resize render area AR to bounds AT.
	 */

	ale_render_resize(ar, at);

	/*
	 * Merge AI into AR with transformation AT.
	 */

	ale_render_merge(ar, at, ai);

	/*
	 * Retain rendered host image DOMAIN_DATA2 from AR.
	 */

	ale_image ari = ale_render_retain_image(ar);

	float *domain_data2 = ale_image_retain_host(ari);

	/*
	 * Display results as text.
	 */

	int di;

	printf("Input data:");
	for (di = 0; di < 27; di++) {
		unsigned int i = domain_data[di];
		printf(" %u", i);
	}
	printf("\n");

	printf("Output data:");
	for (di = 0; di < 27; di++) {
		float f = domain_data2[di];
		printf(" %f", f);
	}
	printf("\n");

	/*
	 * Release dynamic memory.
	 */

	ale_image_release_host(ari, domain_data2);
	ale_image_release(ai);
	ale_image_release(ari);
	ale_trans_release(at);
	ale_render_release(ar);
	ale_context_release(ac);

	/*
	 * Return success (no error).
	 */

	return 0;
}
